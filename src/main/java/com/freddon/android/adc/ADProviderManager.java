package com.freddon.android.adc;


import com.freddon.android.adc.gdt.GDTProvider;
import com.freddon.android.adc.gdt.IGDTProvider;
import com.freddon.android.adc.mi.IMiProvider;
import com.freddon.android.adc.mi.MiProvider;

public class ADProviderManager {

    private static ADProviderManager manager;
    private ADType adType;

    public static ADProviderManager getManager(ADType adType) {
        if (manager == null) {
            manager = new ADProviderManager();
        }
        manager.adType = adType;
        return manager;
    }


    public IGDTProvider getGDTProvider() {
        if (adType == ADType.GDT) {
            return new GDTProvider();
        }
        return null;
    }

//    public IGoogleProvider getGGProvider() {
//        if (adType == ADType.GOOGLE) {
//            return new GoogleProvider();
//        }
//        return null;
//    }

    public IMiProvider getMiProvider() {
        if (adType == ADType.MI) {
            return new MiProvider();
        }
        return null;
    }


//    public IGoogleProvider getGoogleProvider() {
//        return new GoogleProvider();
//    }
}
