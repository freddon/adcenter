package com.freddon.android.adc;

public enum ADType {
    NO(0x0),
    GOOGLE(0x01),
    GDT(0x02),
    CSJ(0x03),
    MI(0x07);

    private int value;
    private ADType(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}
