package com.freddon.android.adc.base;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public interface ADLoadListener<N, S> extends BaseADListener {

    void onADLoaded(List<N> list);

    void onADPreLoaded(AtomicReference<S> reference);
}