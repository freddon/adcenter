package com.freddon.android.adc.base;

public interface ADRewardLoadListener extends BaseADListener {
    void onADPresentComplete();
    void onADDismissed();
}