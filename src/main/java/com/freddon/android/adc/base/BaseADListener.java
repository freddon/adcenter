package com.freddon.android.adc.base;

public interface BaseADListener {

    void onADRenderFailure();

    void onADLoadFailure();

    void onADClicked();

    void onADPresent();
}
