package com.freddon.android.adc.base;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;

public class Meta {

    public static String getStringMetaData(Context context, String keyName) {
        ApplicationInfo appInfo = null;
        try {
            appInfo = context.getPackageManager()
                    .getApplicationInfo(context.getPackageName(),
                            PackageManager.GET_META_DATA);
            return appInfo.metaData.getString(keyName);
        } catch (PackageManager.NameNotFoundException e) {
        }
        return "";
    }
}
