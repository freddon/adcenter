package com.freddon.android.adc.base;

import com.qq.e.ads.nativ.NativeExpressADView;

public interface NativeADLoadListener extends GdtADLoadListener {

    void onADClosed(NativeExpressADView nativeExpressADView);
}
