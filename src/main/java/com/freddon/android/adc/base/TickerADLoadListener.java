package com.freddon.android.adc.base;

public interface TickerADLoadListener<N, S>  extends ADLoadListener <N, S>{
   void onTick(long times);
}
