package com.freddon.android.adc.gdt;

/**
 * 默认，使用前请先初始化
 */
public class GDTConstant {
    public static String APP_ID = "";
    public static String BANNER_ID = "";
    public static String SPLASH_ID = "";
    public static String NATIVE_OIMAGE = "";
    public static String MODAL = "";
    public static String REWARD = "";
}
