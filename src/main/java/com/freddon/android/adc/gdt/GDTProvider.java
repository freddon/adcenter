package com.freddon.android.adc.gdt;

import android.app.Activity;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.freddon.android.adc.base.GdtADLoadListener;
import com.freddon.android.adc.base.GdtTickerADLoadListener;
import com.freddon.android.adc.base.NativeADLoadListener;
import com.freddon.android.admob.conf.ADUnitConfig;
import com.freddon.android.admob.conf.StrategyVO;
import com.qq.e.ads.banner2.UnifiedBannerADListener;
import com.qq.e.ads.banner2.UnifiedBannerView;
import com.qq.e.ads.cfg.BrowserType;
import com.qq.e.ads.cfg.DownAPPConfirmPolicy;
import com.qq.e.ads.cfg.VideoOption;
import com.qq.e.ads.interstitial2.UnifiedInterstitialAD;
import com.qq.e.ads.interstitial2.UnifiedInterstitialADListener;
import com.qq.e.ads.nativ.NativeExpressAD;
import com.qq.e.ads.nativ.NativeExpressADView;
import com.qq.e.ads.rewardvideo.RewardVideoAD;
import com.qq.e.ads.rewardvideo.RewardVideoADListener;
import com.qq.e.ads.splash.SplashAD;
import com.qq.e.ads.splash.SplashADListener;
import com.qq.e.comm.constants.LoadAdParams;
import com.qq.e.comm.util.AdError;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class GDTProvider implements IGDTProvider<StrategyVO> {

    @Override
    public String getAppID() {
        return GDTConstant.APP_ID;
    }

    @Override
    public StrategyVO bundleForBanner(StrategyVO strategyVO) {
        if (strategyVO != null) {
            if (!ADUnitConfig.ADMasterType.GDT.value.equals(strategyVO.getUnitMaster())
                    || !ADUnitConfig.ADUnitType.BANNER.value.equals(strategyVO.getUnitType())
                    || TextUtils.isEmpty(strategyVO.getUnitId())
                    || TextUtils.isEmpty(strategyVO.getUnitAppid())) {
                strategyVO = null;
            }
        }
        if (strategyVO == null) {
            strategyVO = new StrategyVO();
            strategyVO.setUnitAppid(getAppID());
            strategyVO.setUnitId(getBannerUnionID());
            strategyVO.setUnitType(ADUnitConfig.ADUnitType.BANNER.value);
            strategyVO.setUnitMaster(ADUnitConfig.ADMasterType.GDT.value);
        }
        return strategyVO;
    }

    @Override
    public StrategyVO bundleForSplash(StrategyVO strategyVO) {
        if (strategyVO != null) {
            if (!ADUnitConfig.ADMasterType.GDT.value.equals(strategyVO.getUnitMaster())
                    || !ADUnitConfig.ADUnitType.SPLASH.value.equals(strategyVO.getUnitType())
                    || TextUtils.isEmpty(strategyVO.getUnitId())
                    || TextUtils.isEmpty(strategyVO.getUnitAppid())) {
                strategyVO = null;
            }

        }
        if (strategyVO == null) {
            strategyVO = new StrategyVO();
            strategyVO.setUnitAppid(getAppID());
            strategyVO.setUnitId(getSplashUnionID());
        }
        return strategyVO;
    }

    @Override
    public StrategyVO bundleForNative(StrategyVO strategyVO) {
        if (strategyVO != null) {
            if (!ADUnitConfig.ADMasterType.GDT.value.equals(strategyVO.getUnitMaster())
                    || !ADUnitConfig.ADUnitType.STREAM.value.equals(strategyVO.getUnitType())
                    || TextUtils.isEmpty(strategyVO.getUnitId())
                    || TextUtils.isEmpty(strategyVO.getUnitAppid())) {
                strategyVO = null;
            }
        }
        if (strategyVO == null) {
            strategyVO = new StrategyVO();
            strategyVO.setUnitAppid(getAppID());
            strategyVO.setUnitId(getOImageUnionID());
        }
        return strategyVO;
    }

    @Override
    public StrategyVO bundleForModal(StrategyVO strategyVO) {
        if (strategyVO != null) {
            if (!ADUnitConfig.ADMasterType.GDT.value.equals(strategyVO.getUnitMaster())
                    || !ADUnitConfig.ADUnitType.MODAL.value.equals(strategyVO.getUnitType())
                    || TextUtils.isEmpty(strategyVO.getUnitId())
                    || TextUtils.isEmpty(strategyVO.getUnitAppid())) {
                strategyVO = null;
            }
        }
        if (strategyVO == null) {
            strategyVO = new StrategyVO();
            strategyVO.setUnitAppid(getAppID());
            strategyVO.setUnitId(getModalUnionID());
        }
        return strategyVO;
    }

    @Override
    public StrategyVO bundleForReward(StrategyVO strategyVO) {
        if (strategyVO != null) {
            if (!ADUnitConfig.ADMasterType.GDT.value.equals(strategyVO.getUnitMaster())
                    || !ADUnitConfig.ADUnitType.REWARD_VIDEO.value.equals(strategyVO.getUnitType())
                    || TextUtils.isEmpty(strategyVO.getUnitId())
                    || TextUtils.isEmpty(strategyVO.getUnitAppid())) {
                strategyVO = null;
            }
        }
        if (strategyVO == null) {
            strategyVO = new StrategyVO();
            strategyVO.setUnitAppid(getAppID());
            strategyVO.setUnitId(getRewardUnionID());
        }
        return strategyVO;
    }

    @Override
    public String getBannerUnionID() {
        return GDTConstant.BANNER_ID;
    }

    @Override
    public String getModalUnionID() {
        return GDTConstant.MODAL;
    }

    @Override
    public String getRewardUnionID() {
        return GDTConstant.REWARD;
    }

    @Override
    public String getSplashUnionID() {
        return GDTConstant.SPLASH_ID;
    }

    @Override
    public String getOImageUnionID() {
        return GDTConstant.NATIVE_OIMAGE;
    }

    @Override
    public void adForBanner(Activity activity, ViewGroup parent, StrategyVO strategyVO, final GdtADLoadListener adLoadListener) {
        strategyVO = bundleForBanner(strategyVO);
        UnifiedBannerView mBannerView = new UnifiedBannerView(activity, strategyVO.getUnitId(), new UnifiedBannerADListener() {
            @Override
            public void onNoAD(AdError error) {
                Log.i("AD_DEMO", String.format("Banner onNoAD，eCode = %d, eMsg = %s", error.getErrorCode(), error.getErrorMsg()));
                if (adLoadListener != null) {
                    adLoadListener.onADLoadFailure();
                }
            }

            @Override
            public void onADClicked() {

            }

            @Override
            public void onADClosed() {
            }

            @Override
            public void onADExposure() {
                if (adLoadListener != null) {
                    adLoadListener.onADLoaded(null);
                }
            }

            @Override
            public void onADReceive() {
                Log.i("AD_DEMO", "onADReceiv");
            }

            @Override
            public void onADLeftApplication() {

            }

            @Override
            public void onADOpenOverlay() {

            }

            @Override
            public void onADCloseOverlay() {

            }
        });
        if (parent.getChildCount() > 0) {
            parent.removeAllViews();
        }
        parent.addView(mBannerView);
        mBannerView.setRefresh(30);
        /* 发起广告请求，收到广告数据后会展示数据     */
        mBannerView.loadAD();
    }

    @Override
    public void adForSplash(Activity activity, ViewGroup parent, View skipContainer, StrategyVO strategyVO, final GdtTickerADLoadListener adLoadListener, boolean isPreload) {
        strategyVO = bundleForSplash(strategyVO);
        fetchSplashAD(activity, parent, skipContainer, strategyVO.getUnitId(), adLoadListener, 3000, isPreload);
    }


    private NativeExpressADView lastNativeOne;

    @Override
    public void adForNativeAD(Activity activity, final ViewGroup parent, StrategyVO strategyVO, final NativeADLoadListener nativeADLoadListener) {
        if (lastNativeOne != null) {
            lastNativeOne.destroy();
            lastNativeOne = null;
        }
        strategyVO = bundleForNative(strategyVO);
        NativeExpressAD.NativeExpressADListener lis = new NativeExpressAD.NativeExpressADListener() {
            @Override
            public void onADLoaded(List<NativeExpressADView> list) {
                if (list != null && list.size() > 0) {
                    if (lastNativeOne != null) {
                        lastNativeOne.destroy();
                    }
//                    if (lastNativeOne.getBoundData().getAdPatternType() == AdPatternType.NATIVE_VIDEO)
                    lastNativeOne = list.get(0);
                    if (parent.getChildCount() > 0) {
                        parent.removeAllViews();
                    }
                    parent.addView(lastNativeOne);
                    lastNativeOne.render();
                    if (nativeADLoadListener != null) {
                        nativeADLoadListener.onADLoaded(list);
                    }
                }
            }

            @Override
            public void onRenderFail(NativeExpressADView nativeExpressADView) {
                if (nativeADLoadListener != null) {
                    nativeADLoadListener.onADLoadFailure();
                }
            }

            @Override
            public void onRenderSuccess(NativeExpressADView nativeExpressADView) {
                if (nativeADLoadListener != null) {
                    nativeADLoadListener.onADPresent();
                }
            }

            @Override
            public void onADExposure(NativeExpressADView nativeExpressADView) {
            }

            @Override
            public void onADClicked(NativeExpressADView nativeExpressADView) {

            }

            @Override
            public void onADClosed(NativeExpressADView nativeExpressADView) {
                if (nativeADLoadListener != null) {
                    nativeADLoadListener.onADClosed(nativeExpressADView);
                }
            }

            @Override
            public void onADLeftApplication(NativeExpressADView nativeExpressADView) {

            }

            @Override
            public void onADOpenOverlay(NativeExpressADView nativeExpressADView) {

            }

            @Override
            public void onADCloseOverlay(NativeExpressADView nativeExpressADView) {

            }

            @Override
            public void onNoAD(AdError adError) {
                if (nativeADLoadListener != null) {
                    nativeADLoadListener.onADLoadFailure();
                }
            }
        };
        NativeExpressAD nativeExpressAD = new NativeExpressAD(activity, new com.qq.e.ads.nativ.ADSize(com.qq.e.ads.nativ.ADSize.FULL_WIDTH, com.qq.e.ads.nativ.ADSize.AUTO_HEIGHT),
                strategyVO.getUnitId(), lis); // 传入Activity
//        // 注意：如果您在联盟平台上新建原生模板广告位时，选择了支持视频，那么可以进行个性化设置（可选）
        nativeExpressAD.setVideoOption(new VideoOption.Builder()
                .setAutoPlayPolicy(VideoOption.AutoPlayPolicy.WIFI) // WIFI 环境下可以自动播放视频
                .setAutoPlayMuted(true) // 自动播放时为静音
                .build()); //
        nativeExpressAD.setDownAPPConfirmPolicy(DownAPPConfirmPolicy.Default);
        nativeExpressAD.setMaxVideoDuration(60);
        nativeExpressAD.setVideoPlayPolicy(VideoOption.VideoPlayPolicy.AUTO);
        nativeExpressAD.setBrowserType(BrowserType.Default);
        nativeExpressAD.loadAD(1);
    }

    @Override
    public void adForNativeADs(Activity activity, StrategyVO strategyVO, int num, final NativeADLoadListener nativeADLoadListener) {
        strategyVO = bundleForNative(strategyVO);
        NativeExpressAD.NativeExpressADListener lis = new NativeExpressAD.NativeExpressADListener() {
            @Override
            public void onADLoaded(List<NativeExpressADView> list) {
                if (list != null && list.size() > 0) {
//                    if (lastNativeOne.getBoundData().getAdPatternType() == AdPatternType.NATIVE_VIDEO)
                    if (nativeADLoadListener != null) {
                        nativeADLoadListener.onADLoaded(list);
                    }
                }
            }

            @Override
            public void onRenderFail(NativeExpressADView nativeExpressADView) {
                if (nativeADLoadListener != null) {
                    nativeADLoadListener.onADLoadFailure();
                }
            }

            @Override
            public void onRenderSuccess(NativeExpressADView nativeExpressADView) {
                if (nativeADLoadListener != null) {
                    nativeADLoadListener.onADPresent();
                }
            }

            @Override
            public void onADExposure(NativeExpressADView nativeExpressADView) {
            }

            @Override
            public void onADClicked(NativeExpressADView nativeExpressADView) {

            }

            @Override
            public void onADClosed(NativeExpressADView nativeExpressADView) {
                if (nativeADLoadListener != null) {
                    nativeADLoadListener.onADClosed(nativeExpressADView);
                }
            }

            @Override
            public void onADLeftApplication(NativeExpressADView nativeExpressADView) {

            }

            @Override
            public void onADOpenOverlay(NativeExpressADView nativeExpressADView) {

            }

            @Override
            public void onADCloseOverlay(NativeExpressADView nativeExpressADView) {

            }

            @Override
            public void onNoAD(AdError adError) {
                if (nativeADLoadListener != null) {
                    nativeADLoadListener.onADLoadFailure();
                }
            }
        };
        NativeExpressAD nativeExpressAD = new NativeExpressAD(activity, new com.qq.e.ads.nativ.ADSize(com.qq.e.ads.nativ.ADSize.FULL_WIDTH, com.qq.e.ads.nativ.ADSize.AUTO_HEIGHT),
                strategyVO.getUnitId(), lis); // 传入Activity
//        // 注意：如果您在联盟平台上新建原生模板广告位时，选择了支持视频，那么可以进行个性化设置（可选）
        nativeExpressAD.setVideoOption(new VideoOption.Builder()
                .setAutoPlayPolicy(VideoOption.AutoPlayPolicy.WIFI) // WIFI 环境下可以自动播放视频
                .setAutoPlayMuted(true) // 自动播放时为静音
                .build()); //
        nativeExpressAD.setDownAPPConfirmPolicy(DownAPPConfirmPolicy.Default);
        nativeExpressAD.setMaxVideoDuration(60);
        nativeExpressAD.setVideoPlayPolicy(VideoOption.VideoPlayPolicy.AUTO);
        nativeExpressAD.setBrowserType(BrowserType.Default);
        nativeExpressAD.setBrowserType(BrowserType.Default);
        nativeExpressAD.loadAD(num);
    }


    private UnifiedInterstitialAD iad;

    @Override
    public void adForModal(Activity activity, StrategyVO strategyVO, final NativeADLoadListener nativeADLoadListener) {
        if (iad != null) {
            iad.close();
            iad.destroy();
        }
        strategyVO = bundleForModal(strategyVO);
        UnifiedInterstitialADListener lis = new UnifiedInterstitialADListener() {
            @Override
            public void onADReceive() {
                if (nativeADLoadListener != null) {
                    nativeADLoadListener.onADLoaded(null);
                }
                try {
                    if (iad != null) {
                        if (Math.random() > 0.45) {
                            iad.show();
                        } else {
                            iad.showAsPopupWindow();
                        }
                    }
                } catch (Exception e) {
                }

            }

            @Override
            public void onVideoCached() {

            }

            @Override
            public void onNoAD(AdError adError) {
                if (nativeADLoadListener != null) {
                    nativeADLoadListener.onADLoadFailure();
                }
            }

            @Override
            public void onADOpened() {

            }

            @Override
            public void onADExposure() {
                if (nativeADLoadListener != null) {
                    nativeADLoadListener.onADPresent();
                }
            }

            @Override
            public void onADClicked() {
                if (nativeADLoadListener != null) {
                    nativeADLoadListener.onADClicked();
                }
            }

            @Override
            public void onADLeftApplication() {

            }

            @Override
            public void onADClosed() {
                if (iad != null) {
                    iad.destroy();
                }
            }
        };
        VideoOption.Builder builder = new VideoOption.Builder();
        VideoOption option;//= builder.build();
        option = builder.setAutoPlayMuted(true)
                .setAutoPlayPolicy(VideoOption.AutoPlayPolicy.WIFI).build();
        iad = new UnifiedInterstitialAD(activity, strategyVO.getUnitId(), lis);
        iad.setVideoOption(option);
//        iad.setMaxVideoDuration(30);
        /**
         * 如果广告位支持视频广告，强烈建议在调用loadData请求广告前调用setVideoPlayPolicy，有助于提高视频广告的eCPM值 <br/>
         * 如果广告位仅支持图文广告，则无需调用
         */

        /**
         * 设置本次拉取的视频广告，从用户角度看到的视频播放策略<p/>
         *
         * "用户角度"特指用户看到的情况，并非SDK是否自动播放，与自动播放策略AutoPlayPolicy的取值并非一一对应 <br/>
         *
         * 如自动播放策略为AutoPlayPolicy.WIFI，但此时用户网络为4G环境，在用户看来就是手工播放的
         */
        iad.setVideoPlayPolicy(VideoOption.AutoPlayPolicy.WIFI);
        iad.loadAD();
    }

    RewardVideoAD rewardVideoAD = null;

    @Override
    public void adForReward(Activity activity, StrategyVO strategyVO, final NativeADLoadListener nativeADLoadListener) {
        RewardVideoADListener adlistener = new RewardVideoADListener() {
            @Override
            public void onADLoad() {
                try {
                    if (rewardVideoAD != null) {
                        if (!rewardVideoAD.hasShown()) {//广告展示检查2：当前广告数据还没有展示过
                            long delta = 1000;//建议给广告过期时间加个buffer，单位ms，这里demo采用1000ms的buffer
                            //广告展示检查3：展示广告前判断广告数据未过期
                            if (SystemClock.elapsedRealtime() < (rewardVideoAD.getExpireTimestamp() - delta)) {
                                rewardVideoAD.showAD();
                            } else {
                                //激励视频广告已过期
                            }
                        } else {
                            rewardVideoAD.loadAD();
                        }
                    }
                } catch (Exception e) {
                }
                if (nativeADLoadListener != null) {
                    nativeADLoadListener.onADLoaded(null);
                }

            }

            @Override
            public void onVideoCached() {

            }

            @Override
            public void onADShow() {

            }

            @Override
            public void onADExpose() {
                if (nativeADLoadListener != null) {
                    nativeADLoadListener.onADPresent();
                }
            }

            @Override
            public void onReward() {

            }

            @Override
            public void onADClick() {
                if (nativeADLoadListener != null) {
                    nativeADLoadListener.onADClicked();
                }
            }

            @Override
            public void onVideoComplete() {

            }

            @Override
            public void onADClose() {
                if (nativeADLoadListener != null) {
                    nativeADLoadListener.onADClosed(null);
                }
            }

            @Override
            public void onError(AdError adError) {

            }
        };
        rewardVideoAD = new RewardVideoAD(activity,  strategyVO.getUnitId(), adlistener); // 有声播放
        rewardVideoAD.loadAD();
        strategyVO = bundleForModal(strategyVO);
        UnifiedInterstitialADListener lis = new UnifiedInterstitialADListener() {
            @Override
            public void onADReceive() {
                if (nativeADLoadListener != null) {
                    nativeADLoadListener.onADLoaded(null);
                }
                try {
                    if (iad != null) {
                        if (Math.random() > 0.45) {
                            iad.show();
                        } else {
                            iad.showAsPopupWindow();
                        }
                    }
                } catch (Exception e) {
                }

            }

            @Override
            public void onVideoCached() {

            }

            @Override
            public void onNoAD(AdError adError) {
                if (nativeADLoadListener != null) {
                    nativeADLoadListener.onADLoadFailure();
                }
            }

            @Override
            public void onADOpened() {

            }

            @Override
            public void onADExposure() {
                if (nativeADLoadListener != null) {
                    nativeADLoadListener.onADPresent();
                }
            }

            @Override
            public void onADClicked() {
                if (nativeADLoadListener != null) {
                    nativeADLoadListener.onADClicked();
                }
            }

            @Override
            public void onADLeftApplication() {

            }

            @Override
            public void onADClosed() {
                if (iad != null) {
                    iad.destroy();
                }
            }
        };
        VideoOption.Builder builder = new VideoOption.Builder();
        VideoOption option;//= builder.build();
        option = builder.setAutoPlayMuted(true)
                .setAutoPlayPolicy(VideoOption.AutoPlayPolicy.WIFI).build();
        iad = new UnifiedInterstitialAD(activity, strategyVO.getUnitId(), lis);
        iad.setVideoOption(option);
//        iad.setMaxVideoDuration(30);
        /**
         * 如果广告位支持视频广告，强烈建议在调用loadData请求广告前调用setVideoPlayPolicy，有助于提高视频广告的eCPM值 <br/>
         * 如果广告位仅支持图文广告，则无需调用
         */

        /**
         * 设置本次拉取的视频广告，从用户角度看到的视频播放策略<p/>
         *
         * "用户角度"特指用户看到的情况，并非SDK是否自动播放，与自动播放策略AutoPlayPolicy的取值并非一一对应 <br/>
         *
         * 如自动播放策略为AutoPlayPolicy.WIFI，但此时用户网络为4G环境，在用户看来就是手工播放的
         */
        iad.setVideoPlayPolicy(VideoOption.AutoPlayPolicy.WIFI);
        iad.loadAD();
    }


    /**
     * 拉取开屏广告，开屏广告的构造方法有3种，详细说明请参考开发者文档。
     *
     * @param activity      展示广告的 activity
     * @param adContainer   展示广告的大容器
     * @param skipContainer 自定义的跳過按钮：传入该 view 给 SDK 后，SDK 会自动给它绑定点击跳過事件。SkipView 的样式可以由开发者自由定制，其尺寸限制请参考 activity_splash.xml 或下面的注意事项。
     * @param posId         广告位 ID
     * @param loadListener  广告状态监听器
     * @param fetchDelay    拉取广告的超时时长：即开屏广告从请求到展示所花的最大时长（并不是指广告曝光时长）取值范围[3000, 5000]，设为0表示使用广点通 SDK 默认的超时时长。
     */
    private void fetchSplashAD(Activity activity, ViewGroup adContainer, View skipContainer, String posId, final GdtTickerADLoadListener loadListener, int fetchDelay, final boolean preload) {
        final AtomicReference<SplashAD> reference = new AtomicReference<>();
        SplashADListener adListener = new SplashADListener() {
            @Override
            public void onADDismissed() {

            }

            @Override
            public void onNoAD(AdError adError) {
                if (loadListener != null) {
                    loadListener.onADLoadFailure();
                }
            }

            //广告展示后
            @Override
            public void onADPresent() {
                if (loadListener != null) {
                    loadListener.onADPresent();
                }
            }

            @Override
            public void onADClicked() {
                if (loadListener != null) {
                    loadListener.onADClicked();
                }
            }

            //倒计时回调，返回广告还将被展示的剩余时间。
            @Override
            public void onADTick(long l) {
                if (loadListener != null) {
                    loadListener.onTick(l);
                }
            }

            @Override
            public void onADExposure() {
            }

            @Override
            public void onADLoaded(long l) {
                if (loadListener != null) {
                    loadListener.onADLoaded(null);
                }
                if (preload) {
                    if (loadListener != null) {
                        loadListener.onADPreLoaded(reference);
                    }
                }
            }
        };
        SplashAD splashAD = new SplashAD(activity, skipContainer, posId, adListener, fetchDelay);
        reference.set(splashAD);
        if (preload) {
            splashAD.preLoad();
            splashAD.fetchAdOnly();
        } else {
            splashAD.fetchAndShowIn(adContainer);
        }
    }

}
