package com.freddon.android.adc.gdt;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;

import com.freddon.android.adc.base.GdtTickerADLoadListener;
import com.freddon.android.adc.base.IADProvider;
import com.freddon.android.adc.base.GdtADLoadListener;
import com.freddon.android.adc.base.NativeADLoadListener;
import com.freddon.android.admob.conf.StrategyVO;

public interface IGDTProvider<T> extends IADProvider {

    T bundleForBanner(T t);

    T bundleForSplash(T t);

    T bundleForNative(T t);

    T bundleForModal(T t);

    T bundleForReward(T t);

    String getBannerUnionID();

    String getModalUnionID();

    String getRewardUnionID();

    String getSplashUnionID();
    //原生广告
    String getOImageUnionID();

    void adForBanner(Activity activity, ViewGroup parent, StrategyVO strategyVO, GdtADLoadListener adLoadListener);

    void adForSplash(Activity activity, ViewGroup parent, View skipContainer, StrategyVO strategyVO, GdtTickerADLoadListener adLoadListener, boolean isPreload);

    void adForNativeAD(Activity activity, final ViewGroup parent, StrategyVO strategyVO, final NativeADLoadListener nativeADLoadListener);

    void adForNativeADs(Activity activity, StrategyVO strategyVO, int num, NativeADLoadListener nativeADLoadListener);

    void adForModal(Activity activity, StrategyVO strategyVO, NativeADLoadListener nativeADLoadListener);

    void adForReward(Activity activity, StrategyVO strategyVO, NativeADLoadListener nativeADLoadListener);
}
