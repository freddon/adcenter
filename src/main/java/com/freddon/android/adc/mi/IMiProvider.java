package com.freddon.android.adc.mi;

import android.app.Activity;
import android.view.ViewGroup;

import com.freddon.android.adc.base.ADLoadListener;
import com.freddon.android.adc.base.ADRewardLoadListener;
import com.freddon.android.adc.base.IADProvider;
import com.freddon.android.adc.base.NativeADLoadListener;
import com.freddon.android.admob.conf.StrategyVO;

public interface IMiProvider<T> extends IADProvider {

    T bundleForBanner(T t);

    T bundleForSplash(T t);

    T bundleForNative(T t);

    T bundleForModal(T t);

    T bundleForReward(T t);

    String getStreamUnionID();

    String getBannerUnionID();

    String getSplashUnionID();

    String getRewardUnionID();

    String getModalUnionID();

//    void adForStream(Activity activity, ViewGroup parent, ADLoadListener adLoadListener);
//
    void adForSplash(Activity activity, ViewGroup parent,StrategyVO strategyVO, ADLoadListener adLoadListener);
//
//    void adForNativeAD(Activity activity, final ViewGroup parent, final NativeADLoadListener nativeADLoadListener);

    void adForReward(Activity activity, StrategyVO strategyVO, ADRewardLoadListener adRewardLoadListener);

}
