package com.freddon.android.adc.mi;

import com.freddon.android.adc.ADType;

public interface InitListener {

    void success(ADType adType);

    void failure(ADType adType);
}
