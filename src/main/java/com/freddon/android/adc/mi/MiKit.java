package com.freddon.android.adc.mi;

import android.content.Context;

import com.freddon.android.adc.BuildConfig;
import com.miui.zeus.mimo.sdk.MimoSdk;

public class MiKit {

    public static boolean initSuccess = false;

    public static void init(Context context, String appID) {
//        MimoSdk.init(context,appID);
        MimoSdk.init(context);
        MimoSdk.setDebugOn(BuildConfig.DEBUG);
        MimoSdk.setStagingOn(BuildConfig.DEBUG);

    }
}
