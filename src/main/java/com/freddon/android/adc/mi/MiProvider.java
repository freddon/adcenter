package com.freddon.android.adc.mi;

import android.app.Activity;
import android.text.TextUtils;
import android.view.ViewGroup;

import com.freddon.android.adc.base.ADLoadListener;
import com.freddon.android.adc.base.ADRewardLoadListener;
import com.freddon.android.admob.conf.ADUnitConfig;
import com.freddon.android.admob.conf.StrategyVO;
import com.miui.zeus.mimo.sdk.RewardVideoAd;
import com.miui.zeus.mimo.sdk.SplashAd;

public class MiProvider implements IMiProvider<StrategyVO> {

    final ADUnitConfig.ADMasterType currentType = ADUnitConfig.ADMasterType.MI;

    @Override
    public StrategyVO bundleForBanner(StrategyVO strategyVO) {
        if (strategyVO != null) {
            if (!currentType.value.equals(strategyVO.getUnitMaster())
                    || !ADUnitConfig.ADUnitType.BANNER.value.equals(strategyVO.getUnitType())
                    || TextUtils.isEmpty(strategyVO.getUnitId())
                    || TextUtils.isEmpty(strategyVO.getUnitAppid())) {
                strategyVO = null;
            }
        }
        if (strategyVO == null) {
            strategyVO = new StrategyVO();
            strategyVO.setUnitAppid(getAppID());
            strategyVO.setUnitId(getBannerUnionID());
            strategyVO.setUnitType(ADUnitConfig.ADUnitType.BANNER.value);
            strategyVO.setUnitMaster(currentType.value);
        }
        return strategyVO;
    }

    @Override
    public StrategyVO bundleForSplash(StrategyVO strategyVO) {
        if (strategyVO != null) {
            if (!currentType.value.equals(strategyVO.getUnitMaster())
                    || !ADUnitConfig.ADUnitType.SPLASH.value.equals(strategyVO.getUnitType())
                    || TextUtils.isEmpty(strategyVO.getUnitId())
                    || TextUtils.isEmpty(strategyVO.getUnitAppid())) {
                strategyVO = null;
            }

        }
        if (strategyVO == null) {
            strategyVO = new StrategyVO();
            strategyVO.setUnitAppid(getAppID());
            strategyVO.setUnitId(getSplashUnionID());
        }
        return strategyVO;
    }

    @Override
    public StrategyVO bundleForNative(StrategyVO strategyVO) {
        if (strategyVO != null) {
            if (!currentType.value.equals(strategyVO.getUnitMaster())
                    || !ADUnitConfig.ADUnitType.STREAM.value.equals(strategyVO.getUnitType())
                    || TextUtils.isEmpty(strategyVO.getUnitId())
                    || TextUtils.isEmpty(strategyVO.getUnitAppid())) {
                strategyVO = null;
            }
        }
        if (strategyVO == null) {
            strategyVO = new StrategyVO();
            strategyVO.setUnitAppid(getAppID());
            strategyVO.setUnitId(getStreamUnionID());
        }
        return strategyVO;
    }

    @Override
    public StrategyVO bundleForModal(StrategyVO strategyVO) {
        if (strategyVO != null) {
            if (!currentType.value.equals(strategyVO.getUnitMaster())
                    || !ADUnitConfig.ADUnitType.MODAL.value.equals(strategyVO.getUnitType())
                    || TextUtils.isEmpty(strategyVO.getUnitId())
                    || TextUtils.isEmpty(strategyVO.getUnitAppid())) {
                strategyVO = null;
            }
        }
        if (strategyVO == null) {
            strategyVO = new StrategyVO();
            strategyVO.setUnitAppid(getAppID());
            strategyVO.setUnitId(getModalUnionID());
        }
        return strategyVO;
    }

    @Override
    public StrategyVO bundleForReward(StrategyVO strategyVO) {
        if (strategyVO != null) {
            if (!currentType.value.equals(strategyVO.getUnitMaster())
                    || !ADUnitConfig.ADUnitType.REWARD_VIDEO.value.equals(strategyVO.getUnitType())
                    || TextUtils.isEmpty(strategyVO.getUnitId())
                    || TextUtils.isEmpty(strategyVO.getUnitAppid())) {
                strategyVO = null;
            }
        }
        if (strategyVO == null) {
            strategyVO = new StrategyVO();
            strategyVO.setUnitAppid(getAppID());
            strategyVO.setUnitId(getRewardUnionID());
        }
        return strategyVO;
    }

    @Override
    public String getStreamUnionID() {
        return MiConstant.STREAM;
    }

    @Override
    public String getBannerUnionID() {
        return MiConstant.BANNER;
    }

    @Override
    public String getSplashUnionID() {
        return MiConstant.SPLASH_ID;
    }

    @Override
    public String getRewardUnionID() {
        return MiConstant.REWARD;
    }

    @Override
    public String getModalUnionID() {
        return MiConstant.MODAL;
    }

    @Override
    public String getAppID() {
        return MiConstant.APP_ID;
    }


    @Override
    public void adForSplash(Activity activity, ViewGroup parent, StrategyVO strategyVO, ADLoadListener adLoadListener) {
        strategyVO = bundleForNative(strategyVO);
        SplashAd mSplashAd = new SplashAd();
        SplashAd.SplashAdListener mSplashAdListener = new SplashAd.SplashAdListener() {
            @Override
            public void onAdShow() {
                if (adLoadListener != null) {
                    adLoadListener.onADPresent();
                }
            }

            @Override
            public void onAdClick() {
                if (adLoadListener != null) {
                    adLoadListener.onADClicked();
                }
            }

            @Override
            public void onAdDismissed() {
            }

            @Override
            public void onAdLoadFailed(int i, String s) {
                if (adLoadListener != null) {
                    adLoadListener.onADLoadFailure();
                }
            }

            @Override
            public void onAdLoaded() {
                if (adLoadListener != null) {
                    adLoadListener.onADLoaded(null);
                }
            }

            @Override
            public void onAdRenderFailed() {
                if (adLoadListener != null) {
                    adLoadListener.onADRenderFailure();
                }
            }
        };
        mSplashAd.loadAndShow(parent, strategyVO.getUnitId(), mSplashAdListener);
    }


    private RewardVideoAd mRewardVideoAd;

    @Override
    public void adForReward(Activity activity, StrategyVO strategyVO, ADRewardLoadListener adLoadListener) {
        strategyVO = bundleForNative(strategyVO);
        mRewardVideoAd = new RewardVideoAd();
        RewardVideoAd.RewardVideoInteractionListener rewardListener = new RewardVideoAd.RewardVideoInteractionListener() {
            @Override
            public void onAdPresent() {
                if (adLoadListener != null) {
                    adLoadListener.onADPresent();
                }
            }

            @Override
            public void onAdClick() {
                if (adLoadListener != null) {
                    adLoadListener.onADClicked();
                }
            }

            @Override
            public void onAdDismissed() {
                if (adLoadListener != null) {
                    adLoadListener.onADDismissed();
                }
            }

            @Override
            public void onAdFailed(String s) {

            }

            @Override
            public void onVideoStart() {

            }

            @Override
            public void onVideoPause() {

            }

            @Override
            public void onVideoComplete() {
                if (adLoadListener != null) {
                    adLoadListener.onADPresentComplete();
                }
            }

            @Override
            public void onPicAdEnd() {
                if (adLoadListener != null) {
                    adLoadListener.onADPresentComplete();
                }
            }
        };
        mRewardVideoAd.loadAd(strategyVO.getUnitId(), new RewardVideoAd.RewardVideoLoadListener() {
            @Override
            public void onAdRequestSuccess() {

            }

            @Override
            public void onAdLoadSuccess() {
                if (mRewardVideoAd != null) {
                    mRewardVideoAd.showAd(activity, rewardListener);
                }
            }

            @Override
            public void onAdLoadFailed(int errorCode, String errorMsg) {
                if (adLoadListener != null) {
                    adLoadListener.onADLoadFailure();
                }
            }
        });

    }
}
