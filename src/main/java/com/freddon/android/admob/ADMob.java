package com.freddon.android.admob;

import com.freddon.android.admob.conf.APPVersion;
import com.freddon.android.admob.conf.ConfigVO;
import com.freddon.android.admob.conf.HttpDataMessage;
import com.freddon.android.admob.conf.UIOptions;
import com.freddon.android.net.retrofit.RetrofitBuilder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ADMob extends RetrofitBuilder {

    private static ADMob adMob;
    private String appId;
    private String flavor;
    private int version;

    public ADMobApi getAdMobApi() {
        return adMobApi;
    }

    public void setAdMobApi(ADMobApi adMobApi) {
        this.adMobApi = adMobApi;
    }

    private ADMobApi adMobApi;

    private ADMob(String appId, String flavor, int version) {
        super();
        this.appId = appId;
        this.flavor = flavor;
        this.version = version;
        adMobApi = RetrofitBuilder.build(ADMobApi.class);
    }

    public static ADMob newInstance(String appId, String flavor, int version) {
        if (adMob != null) {
            adMob = null;
        }
        adMob = new ADMob(appId, flavor, version);
        return adMob;
    }

//    public void getAdMobConfig(final ADCallBack<ConfigVO> aDCallBack) {
////        gg_ad_rconfig
//        FirebaseRemoteConfig mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
//        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
//                .setMinimumFetchIntervalInSeconds(100)
//                .setFetchTimeoutInSeconds(3)
//                .build();
//        mFirebaseRemoteConfig.setConfigSettingsAsync(configSettings);
//        mFirebaseRemoteConfig.fetchAndActivate()
//                .addOnCompleteListener(task -> {
//                    if (task.isSuccessful()) {
//                        String string = mFirebaseRemoteConfig.getString("gg_ad_rconfig");
//                        try {
//                            ConfigVO config = new Gson().fromJson(string, ConfigVO.class);
//                            if (config != null) {
//                                if ("google".equalsIgnoreCase(config.getFlavorName())) {
//                                    aDCallBack.onReceive(config);
//                                    return;
//                                }
//                            }
//                        } catch (Exception e) {
//                        }
//                        aDCallBack.onReceive(null);
//                    } else {
//                        aDCallBack.onReceive(null);
//                    }
//                });
//    }

    public void getAdMobConfig(final ADCallBack<ConfigVO> aDCallBack) {
        adMobApi.getAdmobConfig(appId, flavor, version).enqueue(this.call(aDCallBack));
    }

    public void getVersionConfig(final ADCallBack<APPVersion> aDCallBack) {
        adMobApi.getVersionConfig(appId, flavor, version).enqueue(this.call(aDCallBack));
    }

    public void getUIConfig(final ADCallBack<UIOptions> aDCallBack) {
        adMobApi.getUIConfig(appId, flavor, version).enqueue(this.call(aDCallBack));
    }


    private <T> Callback<HttpDataMessage<T>> call(final ADCallBack<T> aDCallBack) {
        return new Callback<HttpDataMessage<T>>() {
            @Override
            public void onResponse(Call<HttpDataMessage<T>> call, Response<HttpDataMessage<T>> response) {
                if (response.raw().body() == null) {
                    if (aDCallBack != null) {
                        aDCallBack.onReceive(null);
                    }
                    return;
                }
                HttpDataMessage<T> httpMessage = response.body();
                if (httpMessage != null && httpMessage.getErrorCode() == 0) {
                    T data = httpMessage.getData();
                    if (data != null && aDCallBack != null) {
                        aDCallBack.onReceive(data);
                    }
                } else {
                    if (aDCallBack != null) {
                        aDCallBack.onReceive(null);
                    }
                }
            }

            @Override
            public void onFailure(Call<HttpDataMessage<T>> call, Throwable t) {
                if (aDCallBack != null) {
                    aDCallBack.onReceive(null);
                }
            }
        };
    }

    public interface ADCallBack<T> {
        void onReceive(T t);
    }
}

