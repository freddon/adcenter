package com.freddon.android.admob;

import com.freddon.android.admob.conf.APPVersion;
import com.freddon.android.admob.conf.ConfigVO;
import com.freddon.android.admob.conf.HttpDataMessage;
import com.freddon.android.admob.conf.UIOptions;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ADMobApi {

    @GET("api/v1/gateway/configure")
    Call<HttpDataMessage<ConfigVO>> getAdmobConfig(@Query("appId") String appId, @Query("dimen") String flavor, @Query("ver") int version);

    @GET("api/v1/gateway/composer")
    Call<HttpDataMessage<UIOptions>> getUIConfig(@Query("appId") String appId, @Query("dimen") String flavor, @Query("ver") int version);

    @GET("api/v1/gateway/version")
    Call<HttpDataMessage<APPVersion>> getVersionConfig(@Query("appId") String appId, @Query("dimen") String flavor, @Query("ver") int version);

}