package com.freddon.android.admob;

import com.freddon.android.admob.conf.ADStrategyConfig;
import com.freddon.android.admob.conf.ADUnitConfig;
import com.freddon.android.admob.conf.ConfigVO;
import com.freddon.android.admob.conf.StrategyVO;

import java.util.List;

public class ADMobMatcher {

    /**
     * 命中策略
     *
     * @param anchor
     * @param type
     * @param configVOS
     * @return
     */
    public static StrategyVO hit(ADStrategyConfig.Anchor anchor, ADUnitConfig.ADUnitType type, ConfigVO... configVOS) {
        if (configVOS == null) return null;
        int len = configVOS.length;
        for (int i = 0; i < len; i++) {
            ConfigVO configVO = configVOS[i];
            if (configVO != null) {
                List<StrategyVO> strategies = configVO.getStrategies();
                if (strategies == null || strategies.size() == 0) return null;
                for (StrategyVO strategyVO : strategies) {
                    if (strategyVO == null) continue;
                    if (anchor.value.equals(strategyVO.getAnchorName()) && type.value.equals(strategyVO.getUnitType())) {
                        if (!configVO.isHasAd()) {
                            return null;
                        }
                        return strategyVO;
                    }
                }
            }
        }
        return null;
    }
}
