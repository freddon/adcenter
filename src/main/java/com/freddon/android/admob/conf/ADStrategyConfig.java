package com.freddon.android.admob.conf;

public class ADStrategyConfig {

    public enum Anchor {
        ANYWAY("ANYWAY"),
        SPLASH("SPLASH"),
        LOCK_SCREEN("LOCK_SCREEN"),
        HOME("HOME"),
        HOME_SINGLE("HOME_SINGLE"),
        DETAIL("DETAIL"),
        DETAIL_NEWS("DETAIL_NEWS"),
        TAB_2("TAB_2"),
        TAB_2_SINGLE("TAB_2_SINGLE"),
        NAVIBAR("NAVIBAR"),
        DIALOG_TOP("DIALOG_TOP")
        ;

        public String getValue() {
            return value;
        }

        public String value;

        private Anchor(String type) {
            this.value = type;
        }

    }
}
