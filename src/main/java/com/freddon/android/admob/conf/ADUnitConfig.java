package com.freddon.android.admob.conf;

public class ADUnitConfig {

    public enum ADUnitType {
        SPLASH("SPLASH"),
        BANNER("BANNER"),
        STREAM("STREAM"),
        MODAL("MODAL"),
        REWARD_VIDEO("REWARD_VIDEO"),
        REWARD_OTHER("REWARD_OTHER");

        public String getValue() {
            return value;
        }

        public String value;

        private ADUnitType(String type) {
            this.value = type;
        }

    }

    public enum ADMasterType {
        GDT("GDT"),
        CSJ("CSJ"),
        MI("MI"),
        GOOGLE("GOOGLE"),
        BAIDU("BAIDU"),
        HW("HW");

        public String getValue() {
            return value;
        }

        public String value;

        private ADMasterType(String type) {
            this.value = type;
        }

    }
}
