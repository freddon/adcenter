package com.freddon.android.admob.conf;

import java.io.Serializable;
import java.util.Date;

public class APPVersion implements Serializable {
    private String appIdentifier;

    private String appFlavor;

    private Integer appVersion;

    private String appVersionName;

    private String appStoreUrl;

    private Boolean isForced;

    private Date createdAt;

    private Date updatedAt;

    public String getAppIdentifier() {
        return appIdentifier;
    }

    public void setAppIdentifier(String appIdentifier) {
        this.appIdentifier = appIdentifier == null ? null : appIdentifier.trim();
    }

    public String getAppFlavor() {
        return appFlavor;
    }

    public void setAppFlavor(String appFlavor) {
        this.appFlavor = appFlavor == null ? null : appFlavor.trim();
    }

    public Integer getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(Integer appVersion) {
        this.appVersion = appVersion;
    }

    public String getAppVersionName() {
        return appVersionName;
    }

    public void setAppVersionName(String appVersionName) {
        this.appVersionName = appVersionName == null ? null : appVersionName.trim();
    }

    public String getAppStoreUrl() {
        return appStoreUrl;
    }

    public void setAppStoreUrl(String appStoreUrl) {
        this.appStoreUrl = appStoreUrl == null ? null : appStoreUrl.trim();
    }

    public Boolean getIsForced() {
        return isForced;
    }

    public void setIsForced(Boolean isForced) {
        this.isForced = isForced;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}