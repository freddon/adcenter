package com.freddon.android.admob.conf;

import java.io.Serializable;
import java.util.List;

public class ConfigVO implements Serializable {

    private String id;

    private String appId;

    private String flavorName;

    private String version;

    private boolean hasAd = true;

    private List<StrategyVO> strategies;

    private UIOptions uo;

    private APPVersion av;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getFlavorName() {
        return flavorName;
    }

    public void setFlavorName(String flavorName) {
        this.flavorName = flavorName;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public boolean isHasAd() {
        return hasAd;
    }

    public void setHasAd(boolean hasAd) {
        this.hasAd = hasAd;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<StrategyVO> getStrategies() {
        return strategies;
    }

    public void setStrategies(List<StrategyVO> strategies) {
        this.strategies = strategies;
    }

    public UIOptions getUo() {
        return uo;
    }

    public void setUo(UIOptions uo) {
        this.uo = uo;
    }

    public APPVersion getAv() {
        return av;
    }

    public void setAv(APPVersion av) {
        this.av = av;
    }
}
