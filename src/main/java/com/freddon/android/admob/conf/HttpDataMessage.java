package com.freddon.android.admob.conf;

/**
 * Created by fred on 2018/8/14.
 */
public class HttpDataMessage<T> extends HttpMessage<T> {

    private T data;

    public HttpDataMessage() {
    }

    public HttpDataMessage(T data) {
        this.data = data;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "HttpDataMessage{" +
                "data=" + data +
                "} " + super.toString();
    }
}
