package com.freddon.android.admob.conf;

import java.io.Serializable;

/**
 * Created by fred on 2018/8/13.
 */
public class HttpMessage<T> implements Serializable {

    public final static int CODE_OK = 0;
    public final static int CODE_ERROR = -1;

    private int errorCode;
    private String errorMsg = "";

    public HttpMessage() {
    }

    public HttpMessage(String s) {
        this.errorMsg = s;
    }

    public HttpMessage(int errorCode, String errorMsg) {
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    @Override
    public String toString() {
        return "HttpMessage{" +
                "errorCode=" + errorCode +
                ", errorMsg='" + errorMsg + '\'' +
                '}';
    }
}
