package com.freddon.android.admob.conf;


import java.io.Serializable;

public class StrategyVO implements Serializable {

    private String unitAppid;

    private String unitType;

    private String unitName;

    private String unitMaster;

    private Integer id;

    private String strategyName;

    private String anchorId;

    private String anchorName;

    private String unitId;

    private String appIdentifier;

    private Integer slotPos;


    public String getUnitAppid() {
        return unitAppid;
    }

    public void setUnitAppid(String unitAppid) {
        this.unitAppid = unitAppid;
    }

    public String getUnitType() {
        return unitType;
    }

    public void setUnitType(String unitType) {
        this.unitType = unitType;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getUnitMaster() {
        return unitMaster;
    }

    public void setUnitMaster(String unitMaster) {
        this.unitMaster = unitMaster;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStrategyName() {
        return strategyName;
    }

    public void setStrategyName(String strategyName) {
        this.strategyName = strategyName;
    }

    public String getAnchorId() {
        return anchorId;
    }

    public void setAnchorId(String anchorId) {
        this.anchorId = anchorId;
    }

    public String getAnchorName() {
        return anchorName;
    }

    public void setAnchorName(String anchorName) {
        this.anchorName = anchorName;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getAppIdentifier() {
        return appIdentifier;
    }

    public void setAppIdentifier(String appIdentifier) {
        this.appIdentifier = appIdentifier;
    }

    public Integer getSlotPos() {
        return slotPos;
    }

    public void setSlotPos(Integer slotPos) {
        this.slotPos = slotPos;
    }
}
