package com.freddon.android.admob.conf;

import java.io.Serializable;

public class UIOptions implements Serializable {

    private int isNoAd=0;

    //首页使用自带Splash
    private int splashDefault=1;

    //tab无news模块
    private int homeHasBanner=0;

    //是否有信息流
    private int newsList=1;

    // 0 诱梦  1 uc新闻  2 瑞狮信息流
    private int newsType=0;

    //tab无news模块
    private int tabNoNews=0;

    public int getIsNoAd() {
        return isNoAd;
    }

    public void setIsNoAd(int isNoAd) {
        this.isNoAd = isNoAd;
    }

    public int getSplashDefault() {
        return splashDefault;
    }

    public void setSplashDefault(int splashDefault) {
        this.splashDefault = splashDefault;
    }

    public int getHomeHasBanner() {
        return homeHasBanner;
    }

    public void setHomeHasBanner(int homeHasBanner) {
        this.homeHasBanner = homeHasBanner;
    }

    public int getNewsList() {
        return newsList;
    }

    public void setNewsList(int newsList) {
        this.newsList = newsList;
    }

    public int getNewsType() {
        return newsType;
    }

    public void setNewsType(int newsType) {
        this.newsType = newsType;
    }

    public int getTabNoNews() {
        return tabNoNews;
    }

    public void setTabNoNews(int tabNoNews) {
        this.tabNoNews = tabNoNews;
    }
}
