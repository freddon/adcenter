package com.freddon.android.net.retrofit;


import com.freddon.android.adc.BuildConfig;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory;

/**
 *
 * 引用自 小飞博客 snackBlogs.com
 * Created by fred on 2016/11/2.
 */
public class RetrofitBuilder {


    private static OkHttpClient okHttpClient;

    public static <T> T build(Class<T> service) {
        initOkHttp();
        Retrofit retrofit = new Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(BuildConfig.ADMOB_API_URL)
//                .addConverterFactory(GsonConverterFactory.create())
                .addConverterFactory(StringGsonFactory.create())
                .addCallAdapterFactory(RxJava3CallAdapterFactory.createSynchronous())
                .build();
        return retrofit.create(service);
    }


    /**
     * 初始化okhttp对象
     * @return
     */
    public static OkHttpClient initOkHttp() {
        if (okHttpClient == null) {
            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            if (BuildConfig.DEBUG) {
                HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
                loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
                builder.addInterceptor(loggingInterceptor);
            }
            //设置超时
            builder.connectTimeout(60, TimeUnit.SECONDS);
            builder.readTimeout(60, TimeUnit.SECONDS);
            builder.writeTimeout(60, TimeUnit.SECONDS);
            //错误重连
            builder.retryOnConnectionFailure(true);
            okHttpClient = builder.build();
        }
        return okHttpClient;
    }

}
